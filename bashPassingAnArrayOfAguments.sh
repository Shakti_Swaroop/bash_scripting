#! /bin/bash

#<("$@")> is used when user tries to pass the argument in the form of an Array.
args=("$@")
#Example1
echo the 0th index ${args[0]}
echo the 2nd index ${args[2]}
echo the 1st index ${args[1]}

# To print the entire array at once
#Example2
echo The array contains following values: $@

#To know the number values passed in the Array we use <$#>.
#Example3
echo number of arguments passed : $#
