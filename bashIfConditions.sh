#! /bin/bash

## Integer Comparison

# -eq - is equal to - if ["$a" -eq "#b"]
# -ne - not equal to - if ["$a" -ne "#b"]
# -gt - is greater than - if ["$a" -gt "#b"]
# -ge - is greater/equal to - if ["$a" -ge "#b"]
# -lt - is lesser than - if ["$a" -lt "#b"]
# -le - is lesser/equal to - if ["$a" -le "#b"]

# < - is lesser than - if (("$a" < "#b"))
# <= - is lesser/equal to - if (("$a" <= "#b"))
# > - is greater than - if (("$a" > "#b"))
# >= - is greater/equal to - if (("$a" >= "#b"))

## String Comparison

# == - is equal to - if ["$a" == "$b"]
# = - is equal to - if ["$a" = "$b"]
# != - is not equal to - if ["$a" != "$b"]
# < - is lesser than - if ["$a" < "#b"]
# > - is greater than - if ["$a" > "#b"]
# -z - is null , has zero length - if ["$a" -z "#b"]


read -p "Enter any two integers. " -a integer




if [ "${integer[0]}" = "${integer[1]}" ]
then
  echo -"Their value are same."
elif [ "${integer[0]}" \< "${integer[1]}" ]
   then
     echo - "${integer[0]} is lesser than ${integer[1]}"
elif [ "${integer[0]}" \> "${integer[1]}" ]
then
    echo - "${integer[0]} is greater than ${integer[1]}"
fi

read -p "Enter any two integers. " -a string




if [ "${string[0]}" = "${string[1]}" ]
then
  echo -"Their value are same."
elif [ "${string[0]}" \< "${string[1]}" ]
   then
     echo - "${string[0]} is lesser than ${string[1]}"
elif [ "${string[0]}" \> "${string[1]}" ]
then
    echo - "${string[0]} is greater than ${string[1]}"
fi
