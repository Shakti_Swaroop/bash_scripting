#! /bin/bash

# <$1 $2 $3....> are reserved for default variables and are assigned when user tries to pass some valua are argument
# $0 is reserved for script name
#Example1
echo "Script name : $0"
echo "First argument is : $1"
echo "Second argumentis : $2"

# <$#> give the count of argument passed
echo "number of arguments passed $#"
