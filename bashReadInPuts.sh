#! /bin/bash

# <read> command is used for getting inputs from terminal

#Example1
echo what people call you?
read name
echo your name is $name




# To skip printing of the in put while he types it we use <-s> .Let say you dont want print the password a user types.

#Example2
echo "what is your age?"
read -s age
echo "you are $age years old."

#Example3
echo Please type your full name as FIRST MIDDLE and LASD Name.
read first middle last
echo Your full name is $first $middle $last.

# To print the input on the same line we use a flag <-p>.
# To skip printing of the in put while he types it we use <-s>.Let say you dont want print the password a user types.
#Example4
read -p 'UserName? ' username
echo You typed your name as $name.
read -sp Password? password
echo You typed your $password

# To store values in the format of an Array we use flag <-a>.
#Example5
echo "Enter the name of your family members : "
read -a name
echo "The first three names you have entered are ${name[0]}, ${name[1]}, ${name[2]}"


# How to extract all the various stored in an array
#Example6
read -p 'Enter the names of all your family members: ' -a names
echo "your first family members is ${names[0]}"
echo "your second family members is ${names[1]}"
echo "your Third family members is ${names[2]}"

# <REPLY> is the built in variable which used in case the user doesn't explicitly declare a name
#Example7
read -p "What should be the value for reply : "
echo "You typed $REPLY"

# <REPLY> cant be used as default variable name in case it is of array type .
#example8
read -p "What array of values you want store in REPLY : "
echo "The 1th value is ${REPLY[1]}"
